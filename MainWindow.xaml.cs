﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;
using ESpritePacker.Core;
using System.Windows.Input;

namespace ESpritePacker
{
    public partial class MainWindow : Window
    {
        private BackgroundWorker _worker;
        private string[] _spriteNames;
        private BuildProperties _buildProps;
        private bool _error;

        public MainWindow() {
            InitializeComponent();
            AllowDrop = true;
            _worker = new BackgroundWorker { WorkerReportsProgress = true };
            _worker.DoWork += worker_DoWork;
            _worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            _worker.ProgressChanged += worker_ProgressChanged;
            StatusBar.Text = "Welcome";
        }

        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            ProgressBar.Value = e.ProgressPercentage;
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e) {
            try {
                Packaging.Merge(_spriteNames, _buildProps, ref _worker);
            } catch (Exception ex) {
                _error = true;
                Dispatcher.Invoke(delegate () {
                    StatusBar.Text = ex.Message;
                });
            }
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            if (FastMode.IsChecked != null && (bool)FastMode.IsChecked) {
                ClearTheList();
            }
            if (!_error)
                StatusBar.Text = "Sprite Sheet Created Successfully.";
        }

        private void RemoveAllSpritesButton_Click(object sender, RoutedEventArgs e) {
            ClearTheList();
        }

        private void ClearTheList() {
            SelectedSpritesListBox.Items.Clear();
            SpriteSelectionSizeChanged();
        }

        private void AddNewSpritesButton_Click(object sender, RoutedEventArgs e) {
            OpenAddSpriteDialog();
        }

        private void OpenAddSpriteDialog() {
            var openFileDialog = new OpenFileDialog {
                DefaultExt = ".png",
                Filter = "Images (.png)|*.png",
                Multiselect = true
            };
            // Default file extension
            // Filter files by extension
            if (openFileDialog.ShowDialog() == true) {
                var spritesName = openFileDialog.FileNames;
                foreach (var spriteName in spritesName) {
                    if (System.IO.Path.GetExtension(spriteName)?.ToLower() != ".png") continue;

                    var t = new ListBoxItem { Content = spriteName };
                    SelectedSpritesListBox.Items.Add(t);
                }
            }
            if (FastMode.IsChecked != null && (bool)FastMode.IsChecked) {
                StartPacking();
            }
        }

        private void RemoveSelectedSpritesButton_Click(object sender, RoutedEventArgs e) {
            if (SelectedSpritesListBox.Items.Count <= 0) return;
            var temp = new ListBoxItem[SelectedSpritesListBox.SelectedItems.Count];
            SelectedSpritesListBox.SelectedItems.CopyTo(temp, 0);
            foreach (var t in temp) {
                SelectedSpritesListBox.Items.Remove(t);
            }
            SpriteSelectionSizeChanged();
        }

        private void BrowseForFileLocationButton_Click(object sender, RoutedEventArgs e) {
            // Configure save file dialog box
            var saveDialog = new SaveFileDialog {
                FileName = "SpriteSheet",
                DefaultExt = ".png",
                Filter = "Images (.png)|*.png"
            };
            // Default file name
            // Default file extension
            // Filter files by extension

            try {
                var tmp = saveDialog.ShowDialog();
                if (tmp == true) {
                    // Save document
                    OutputLocation.Text = saveDialog.FileName;
                }
            } catch (Exception exception) {
                Console.WriteLine(exception);
                throw;
            }
        }

        private void PackThemButton_Click(object sender, RoutedEventArgs e) {
            StartPacking();
        }

        private void StartPacking() {
            try {
                ProgressBar.Value = 0;
                _buildProps = new BuildProperties {
                    Row = (int)Rows.Value,
                    Column = (int)Columns.Value,
                    GenerateMap = GenerateMap.IsChecked != null && (bool)GenerateMap.IsChecked,
                    OutputPath = UseCustomDirectory.IsChecked != null && (bool)UseCustomDirectory.IsChecked ? OutputLocation.Text : string.Empty,
                    Padding = (byte)SpritePadding.Value
                };

                var i = 0;
                _spriteNames = new string[SelectedSpritesListBox.Items.Count];
                foreach (var item in SelectedSpritesListBox.Items) {
                    _spriteNames[i++] = ((ListBoxItem)item).Content as string;
                }
                _worker.RunWorkerAsync();
            } catch (Exception ex) {
                StatusBar.Text = ex.Message;
            }
            StatusBar.Text = "Creating Sprite Sheet...";
        }

        private void DropHere_Drop(object sender, DragEventArgs e) {
            if (e.Data.GetData(DataFormats.FileDrop, true) is string[] droppedFileNames)
                foreach (var s in droppedFileNames) {
                    if (System.IO.Path.GetExtension(s)?.ToLower() != ".png") continue;

                    var t = new ListBoxItem { Content = s };
                    SelectedSpritesListBox.Items.Add(t);
                }

            SpriteSelectionSizeChanged();
            if (FastMode.IsChecked != null && (bool)FastMode.IsChecked) {
                StartPacking();
            }
        }

        private void Rows_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
            if (CustomSize.IsChecked != null && (Columns == null || Rows == null || !(bool)CustomSize.IsChecked)) return;
            var count = SelectedSpritesListBox.Items.Count;
            Columns.Value = Math.Ceiling(count / Rows.Value);
        }

        private void Columns_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
            if (CustomSize.IsChecked != null && (Columns == null || Rows == null || !(bool)CustomSize.IsChecked)) return;
            var count = SelectedSpritesListBox.Items.Count;
            Rows.Value = Math.Ceiling(count / Columns.Value);
        }

        private void SelectedSpritesListBox_SizeChanged(object sender, SizeChangedEventArgs e) {
            SpriteSelectionSizeChanged();
        }

        private void SpriteSelectionSizeChanged() {
            var count = SelectedSpritesListBox.Items.Count;
            var sqrt = Math.Sqrt(count);
            if (Math.Abs(count % sqrt) < 0.001f) {
                Columns.Value = sqrt;
                Rows.Value = sqrt;
            } else {
                var tmp = Math.Ceiling(sqrt);
                Columns.Value = tmp;
                Rows.Value = tmp;
            }
        }

        private void CustomSize_Unchecked(object sender, RoutedEventArgs e) {
            var count = SelectedSpritesListBox.Items.Count;
            var sqrt = Math.Sqrt(count);
            if (Math.Abs(count % sqrt) < 0.001f) {
                Columns.Value = sqrt;
                Rows.Value = sqrt;
            } else {
                var tmp = Math.Floor(sqrt) + 1;
                Columns.Value = tmp;
                Rows.Value = tmp;
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.F2) {
                ClearTheList();
            } else if (e.Key == Key.F3) {
                GenerateMap.IsChecked = !GenerateMap.IsChecked;
            } else if (e.Key == Key.F4) {
                CustomSize.IsChecked = !CustomSize.IsChecked;
            } else if (e.Key == Key.F5) {
                OpenAddSpriteDialog();
            } else if (e.Key == Key.F12) {
                StartPacking();
            }
        }

        private void Expander_Expanded(object sender, RoutedEventArgs e) {
            Height = MaxHeight;
        }

        private void Expander_Collapsed(object sender, RoutedEventArgs e) {
            Height = MinHeight;
        }

        private void About_Click(object sender, RoutedEventArgs e) {
            var about = new About();
            about.Show();
        }

        private void Close_Click(object sender, RoutedEventArgs e) {
            Close();
        }
    }
}