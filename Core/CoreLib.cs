﻿using System;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.ComponentModel;
using System.Diagnostics;

namespace ESpritePacker.Core {
    public class Packaging {
        public static void Merge(string[] sprites, BuildProperties properties, ref BackgroundWorker worker) {

            long spritesTotalLength = 0;
            var padding = properties.Padding;

            var atlasFileParentDirectoryName = Directory.GetParent(sprites[0]).Name;
            var atlasFileParentDirectoryPath = Path.GetDirectoryName(sprites[0]);
            string atlasFilePath;
            string mapFilePath;

            var spritePositions = new Point[sprites.Length];
            var testImage = Image.FromFile(sprites[0]);
            var atlas = new Bitmap(testImage.Width * properties.Column + padding * (properties.Column + 1), testImage.Height * properties.Row + padding * (properties.Row + 1), PixelFormat.Format32bppArgb);

            #region SAVE ATLAS

            if (padding != 0) {
                //setting the hole image with transparent color
                for (var i = 0; i < atlas.Height; i++) {
                    for (var j = 0; j < atlas.Width; j++) {
                        atlas.SetPixel(j, i, Color.Transparent);
                    }
                }
                worker.ReportProgress(50);
            }

            Bitmap currentSprite;
            int index;
            for (var j = 0; j < properties.Row; j++) {
                for (var i = 0; i < properties.Column; i++) {
                    index = j * properties.Column + i;
                    if (index >= sprites.Length)
                        break;
                    Debug.WriteLine(index);
                    currentSprite = Image.FromFile(sprites[index]) as Bitmap;

                    spritesTotalLength += new FileInfo(sprites[index]).Length;

                    if (currentSprite == null)
                        throw new FileNotFoundException();

                    spritePositions[index] = new Point(padding * (i + 1) + i * currentSprite.Width, padding * (j + 1) + j * currentSprite.Height);

                    var width = currentSprite.Width;
                    var height = currentSprite.Height;
                    var xOffset = spritePositions[index].X;
                    var yOffset = spritePositions[index].Y;

                    for (var y = 0; y < height; y++) {
                        for (var x = 0; x < width; x++) {
                            atlas.SetPixel(xOffset + x, yOffset + y, currentSprite.GetPixel(x, y));
                        }
                    }
                    if (padding != 0)
                        worker.ReportProgress(50 + (index + 1) * 50 / sprites.Length);
                    else
                        worker.ReportProgress((index + 1) * 100 / sprites.Length);
                }
            }

            atlasFilePath = string.IsNullOrEmpty(properties.OutputPath) ? $@"{atlasFileParentDirectoryPath}\{atlasFileParentDirectoryName}.png" : properties.OutputPath;

            atlas.Save(atlasFilePath, ImageFormat.Png);
            #endregion

            #region GENERATE MAP

            if (properties.GenerateMap == false) return;

            mapFilePath = string.IsNullOrEmpty(properties.OutputPath) ? $@"{atlasFileParentDirectoryPath}\{atlasFileParentDirectoryName}.txt" : $@"{Path.GetDirectoryName(properties.OutputPath)}\{Path.GetFileNameWithoutExtension(properties.OutputPath)}.txt";

            using (var file = new StreamWriter(mapFilePath)) {

                var currentSize = new FileInfo(atlasFilePath).Length;
                float difference = spritesTotalLength - currentSize;

                file.WriteLine("Summery");
                file.WriteLine("\tEach Section : " + testImage.Width + " x " + testImage.Height);
                file.WriteLine("\tSprite Sheet Size : " + atlas.Size.Width + " x " + atlas.Size.Height);
                file.WriteLine("\tBefore : " + (spritesTotalLength / 1024f).ToString("00.00") + " KB");
                file.WriteLine("\tNow : " + (currentSize / 1024f).ToString("00.00") + " KB");
                file.WriteLine("\tSize Difference : " + (difference / 1024f).ToString("00.00") + " KB");
                file.WriteLine("");

                for (var i = 0; i < spritePositions.Length; i++) {
                    file.WriteLine("\t" + Path.GetFileNameWithoutExtension(sprites[i]) +
                                   $"\t[ x = {spritePositions[i].X:0000} ,y = {spritePositions[i].Y:0000} ]");
                }

                file.WriteLine("------------------------------------------------------");
                file.WriteLine("Unity Version of Coordinates\r\n");

                for (var i = 0; i < spritePositions.Length; i++) {
                    file.WriteLine("\t" + Path.GetFileNameWithoutExtension(sprites[i]) +
                                   $"\t[ x = {spritePositions[i].X:0000} ,y = {(atlas.Height - spritePositions[i].Y - testImage.Height):0000} ]");
                }
                file.WriteLine("");
                file.WriteLine("Developer : e.mottaghi.rezaei@gmail.com\n");
                file.WriteLine("Generated By ESpritePacker© v1.3.0.1 On "+ DateTime.Now.ToLongDateString());
            }

            #endregion
        }
    }
}