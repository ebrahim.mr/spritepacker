﻿namespace ESpritePacker.Core {
    public class BuildProperties {
        public int Row;
        public int Column;
        public byte Padding;
        public string OutputPath;
        public bool GenerateMap;

        public BuildProperties() {
            Row = 1;
            Column = 2;
            OutputPath = string.Empty;
            GenerateMap = true;
            Padding = 0;
        }
    }
}
